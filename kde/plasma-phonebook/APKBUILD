# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-phonebook
pkgver=0_git20191010
pkgrel=0
_commit="013de6a99966e87f7e36cfadedec8a7ac07be7fb"
pkgdesc="Contacts application which allows adding, modifying and removing contacts"
arch="all"
url="https://invent.kde.org/kde/plasma-phonebook"
license="GPL-2.0-only OR GPL-3.0-only"
depends="kirigami2"
makedepends="extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev qt5-qtquickcontrols2-dev kirigami2-dev kpeople-dev kcontacts-dev"
source="$pkgname-$_commit.tar.gz::https://invent.kde.org/kde/plasma-phonebook/-/archive/$_commit.tar.gz"
options="!check" # No tests
builddir="$srcdir/$pkgname-$_commit"

prepare() {
	default_prepare

	mkdir "$builddir"/build
}

build() {
	cd "$builddir"/build
	cmake "$builddir" \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	make
}

check() {
	cd "$builddir"/build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"/build
	DESTDIR="$pkgdir" make install
}
sha512sums="d7cfd2ea89d48d0cf5bf16c6510001442c9aed967e3f609aecdb66b234308c1f9008ca4fb241635bff9905f86796579517fcda269813cb69f2aaaca56c643474  plasma-phonebook-013de6a99966e87f7e36cfadedec8a7ac07be7fb.tar.gz"
